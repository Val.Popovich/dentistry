document.querySelector('.burger-menu__btn').addEventListener('click', function () {
    let menu =  document.querySelector('.burger-menu__navigation');
    if (menu.style.display === 'none') {
        menu.style.display = 'flex';
    } else {
        menu.style.display = 'none'
    }
});
