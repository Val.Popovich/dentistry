let offset = 0;
let wrapperWidth = 0;
const sliderItems = document.querySelector('.services__items');
const commandSliderItems = document.querySelector('.command__items');


document.querySelector('#slider__next-btn').addEventListener('click', function(){
    wrapperWidth = document.querySelector('.command__slider-wrapper').offsetWidth;
    offset = offset + wrapperWidth;
    if (offset > wrapperWidth) {
        offset = 0;
    }
    sliderItems.style.left = -offset+ 'px';
});

document.querySelector('#slider__prev-btn').addEventListener('click', function () {
    wrapperWidth = document.querySelector('.command__slider-wrapper').offsetWidth;
    offset = offset - wrapperWidth;
    if (offset < 0) {
        offset = wrapperWidth;
    }
    sliderItems.style.left = -offset + 'px';
});


document.querySelector('#command-slider__next-btn').addEventListener('click', function(){
    wrapperWidth = document.querySelector('.command__slider-wrapper').offsetWidth;
    console.log(wrapperWidth)
    offset = offset + wrapperWidth;
    if (offset > wrapperWidth) {
        offset = 0;
    }
    commandSliderItems.style.left = -offset + 'px';
});

document.querySelector('#command-slider__prev-btn').addEventListener('click', function () {
    wrapperWidth = document.querySelector('.command__slider-wrapper').offsetWidth;
    offset = offset - wrapperWidth;
    if (offset < 0) {
        offset = wrapperWidth;
    }
    commandSliderItems.style.left = -offset + 'px';
});



