function setIsModalHidden() {
    const modal = document.querySelector('.sign-in-modal');
    modal.classList.toggle('_hidden');
}

function closeAllModals() {
    const modal = document.querySelector('.sign-in-modal');
    if (!modal.classList.contains('_hidden')) {
        modal.classList.add('_hidden');

    }

    const secondModal = document.querySelector('.register-modal');
    if (!secondModal.classList.contains('_hidden')) {
        secondModal.classList.add('_hidden');
    }
}

function setIsSecondModalHidden() {
    const signInModal = document.querySelector('.sign-in-modal');
    signInModal.classList.toggle('_hidden');

    const modal = document.querySelector('.register-modal');
    modal.classList.toggle('_hidden');
}

function toggleModals() {
    document.querySelector('.sign-in-modal').classList.toggle('_hidden');
    document.querySelector('.register-modal').classList.toggle('_hidden');
}



